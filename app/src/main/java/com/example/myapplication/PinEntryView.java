package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PinEntryView extends Activity {

    String userEntered;
    String pass = "1234";

    final int PIN_LENGTH = 4;
    boolean keyPadLockedFlag = false;
    Context appContext;

    TextView titleView;

    TextView pinBox0;
    TextView pinBox1;
    TextView pinBox2;
    TextView pinBox3;


    TextView statusView;

    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;

    Button buttonExit;
    Button buttonDelete;
    EditText passInput;
    ImageView backSpace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appContext = this;
        userEntered = "";


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_pin_entry_view);

        statusView = (TextView) findViewById(R.id.statusview);
        passInput = (EditText) findViewById(R.id.editText);
        backSpace = (ImageView) findViewById(R.id.imageView);
        buttonExit = (Button) findViewById(R.id.buttonExit);
        backSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passInput.setText(passInput.getText().toString().substring(0, passInput.getText().toString().length() - 2));
            }
        });
        buttonExit.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View v) {

                                              //Exit app
                                              Intent i = new Intent();
                                              i.setAction(Intent.ACTION_MAIN);
                                              i.addCategory(Intent.CATEGORY_HOME);
                                              appContext.startActivity(i);
                                              finish();

                                          }

                                      }
        );

        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View v) {

                                                if (keyPadLockedFlag == true) {
                                                    return;
                                                }

                                                if (userEntered.length() > 0) {
                                                    userEntered = userEntered.substring(0, userEntered.length() - 1);
                                                    passInput.setText("");
                                                }


                                            }

                                        }
        );

        titleView = (TextView) findViewById(R.id.time);

        View.OnClickListener pinButtonHandler = new View.OnClickListener() {
            public void onClick(View v) {

                if (keyPadLockedFlag == true) {
                    return;
                }

                Button pressedButton = (Button) v;


                if (userEntered.length() < PIN_LENGTH) {
                    userEntered = userEntered + pressedButton.getText();
                    Log.v("PinView", "User entered=" + userEntered);

                    passInput.setText(passInput.getText().toString() + "*");
                    passInput.setSelection(passInput.getText().toString().length());

                    if (userEntered.length() == PIN_LENGTH) {
                        if (userEntered.equals(pass)) {
                            statusView.setTextColor(Color.GREEN);
                            statusView.setText("Correct");
                            Log.v("PinView", "Correct PIN");
                            Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(myIntent);
                        } else {
                            statusView.setTextColor(Color.RED);
                            statusView.setText("Wrong PIN. Keypad Locked");
                            keyPadLockedFlag = true;
                            Log.v("PinView", "Wrong PIN");

                            new LockKeyPadOperation().execute("");
                        }
                    }
                } else {
                    passInput.setText("");

                    userEntered = "";

                    statusView.setText("");

                    userEntered = userEntered + pressedButton.getText();
                    Log.v("PinView", "User entered=" + userEntered);

                    passInput.setText("8");
                }
            }
        };


        btn0 = (Button) findViewById(R.id.button0);
        btn0.setOnClickListener(pinButtonHandler);

        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(pinButtonHandler);

        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(pinButtonHandler);

        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(pinButtonHandler);

        btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(pinButtonHandler);

        btn5 = (Button) findViewById(R.id.button5);
        btn5.setOnClickListener(pinButtonHandler);

        btn6 = (Button) findViewById(R.id.button6);
        btn6.setOnClickListener(pinButtonHandler);

        btn7 = (Button) findViewById(R.id.button7);
        btn7.setOnClickListener(pinButtonHandler);

        btn8 = (Button) findViewById(R.id.button8);
        btn8.setOnClickListener(pinButtonHandler);

        btn9 = (Button) findViewById(R.id.button9);
        btn9.setOnClickListener(pinButtonHandler);


        buttonDelete = (Button) findViewById(R.id.buttonDeleteBack);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    private class LockKeyPadOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 2; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            statusView.setText("");

            //Roll over
            passInput.setText("");
            ;

            userEntered = "";

            keyPadLockedFlag = false;
            if (userEntered.equals(pass))
            {
                statusView.setTextColor(Color.GREEN);
                statusView.setText("Correct");
                Log.v("PinView", "Correct PIN");
                Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(myIntent);
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}