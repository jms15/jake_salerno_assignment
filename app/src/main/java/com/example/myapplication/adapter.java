package com.example.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class adapter extends RecyclerView.Adapter<adapter.viewHolder> {
    private Context mContext;
    private ArrayList<ItemActivity> mExampleList;

    public adapter(Context context, ArrayList<ItemActivity> exampleList)
    {
        mContext = context;
        mExampleList = exampleList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.itemlayout, parent, false);
        return new viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ItemActivity currentItem = mExampleList.get(position);

        String creatorName = currentItem.getCreator();
        int likeCount = currentItem.getLikeCount();

        holder.mTextViewCreator.setText(creatorName);
        holder.mTextViewLikes.setText("Likes: "+likeCount);
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder
    {
        public TextView mTextViewCreator;
        public TextView mTextViewLikes;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            mTextViewLikes = itemView.findViewById(R.id.text_view_likes);
            mTextViewCreator = itemView.findViewById(R.id.text_view_creator);
        }
    }

}
