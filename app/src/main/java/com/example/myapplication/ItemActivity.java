package com.example.myapplication;

public class ItemActivity {
    private String mCreator;
    private int mLikes;

    public ItemActivity(String creator, int likes)
    {
        mCreator = creator;
        mLikes = likes;
    }

    public String getCreator()
    {
        return mCreator;
    }

    public int getLikeCount()
    {
        return mLikes;
    }
}
