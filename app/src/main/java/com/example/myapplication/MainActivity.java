package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "before";
    private String dateText;

    private EditText Name;
    private EditText Surname;
    private TextView Output;
    private Button submit;

    public String nameString;
    public String surnameString;


    @Override
    protected void onStop()
    {
        super.onStop();
        Calendar cal = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance().format(cal.getTime());
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");

        //Date
        SharedPreferences datePrefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = datePrefs.edit();
        editor.putString(TEXT, "The Date is "+currentDate+" and The Time is "+time.format(cal.getTime()));
        editor.commit();

        //Form
        SharedPreferences sharedPrefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        SharedPreferences.Editor nameEditor = sharedPrefs.edit();
        nameEditor.putString("name", Name.getText().toString());
        nameEditor.putString("surname", Surname.getText().toString());
        nameEditor.commit();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Calendar cal = Calendar.getInstance();
        String currentDate = DateFormat.getDateInstance().format(cal.getTime());
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");

        //Date
        SharedPreferences datePrefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = datePrefs.edit();
        editor.putString(TEXT, "The Date is "+currentDate+" and The Time is "+time.format(cal.getTime()));
        editor.commit();

        //Form
        SharedPreferences sharedPrefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        SharedPreferences.Editor nameEditor = sharedPrefs.edit();
        nameEditor.putString("name", Name.getText().toString());
        nameEditor.putString("surname", Surname.getText().toString());
        nameEditor.commit();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        //Date
        SharedPreferences datePrefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        dateText = datePrefs.getString(TEXT, "");
        Snackbar.make(findViewById(R.id.drawer_layout), dateText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


        //Form
        SharedPreferences sharedPrefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        nameString = sharedPrefs.getString("name", "");
        surnameString = sharedPrefs.getString("surname", "");
        Output.setText("Current User: "+ nameString+" "+surnameString);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Date
        SharedPreferences datePrefs = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        dateText = datePrefs.getString(TEXT, "");
        Snackbar.make(findViewById(R.id.drawer_layout), dateText, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();


        //Form
        SharedPreferences sharedPrefs = getSharedPreferences("userInfo", MODE_PRIVATE);
        nameString = sharedPrefs.getString("name", "");
        surnameString = sharedPrefs.getString("surname", "");
        Output.setText("Current User: "+ nameString+" "+surnameString);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        Name = (EditText) findViewById(R.id.eT_name);
        Surname = (EditText) findViewById(R.id.eT_surname);
        Output = (TextView) findViewById(R.id.txt_savedData);
        submit = (Button) findViewById(R.id.btn_loadData);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                SharedPreferences sharedPrefs = getSharedPreferences("userInfo", MODE_PRIVATE);
                SharedPreferences.Editor nameEditor = sharedPrefs.edit();
                nameEditor.putString("name", Name.getText().toString());
                nameEditor.putString("surname", Surname.getText().toString());
                nameEditor.commit();

                nameString = sharedPrefs.getString("name", "");
                surnameString = sharedPrefs.getString("surname", "");
                Output.setText("Current User: "+ nameString+" "+surnameString);

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.hometitle) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.favorites) {
            Intent myIntent = new Intent(getBaseContext(), itemHolder.class);
            startActivity(myIntent);


        } else if (id == R.id.logout) {
            finishAffinity();
            Intent myIntent = new Intent(getBaseContext(), fingerprint.class);
            startActivity(myIntent);

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
